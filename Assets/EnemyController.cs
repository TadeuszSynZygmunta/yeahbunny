﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour
{

    public bool faceRight = false, touchWall = false, touchFloor = true;
    public float setRunSpeed, raycastsLenght;

    float runSpeed;
    int faceInt = -1;

    bool blockEdgeCheck = false;   

    Rigidbody2D rigid;
    Animator anim;
    RaycastHit2D leftDownHit, rightDownHit;
    Vector2 leftDown, rightDown;
    PlayerController playerController;

    void Awake()
    {
        rigid = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        playerController = FindObjectOfType<PlayerController>();
    }

    void Start()
    {
        runSpeed = setRunSpeed;
    }

    void Update()
    {
        MoveLeftAndRight();
        Raycasts();

        if (!touchFloor && !blockEdgeCheck)
        {
            touchWall = true;
            StartCoroutine("BlockEdgeDetecting");    
        }
    }

    void Raycasts()
    {
        leftDown = new Vector2(transform.position.x - 4, transform.position.y - 6);
        rightDown = new Vector2(transform.position.x + 4, transform.position.y - 6);

        leftDownHit = Physics2D.Raycast(leftDown, Vector2.down, raycastsLenght);
        rightDownHit = Physics2D.Raycast(rightDown, Vector2.down, raycastsLenght);
        
        Debug.DrawLine(leftDown, new Vector2(leftDown.x, leftDown.y - raycastsLenght), Color.blue);
        Debug.DrawLine(rightDown, new Vector2(rightDown.x, rightDown.y - raycastsLenght), Color.blue);

        if ((leftDownHit.collider != null && leftDownHit.collider.CompareTag("Floor")) &&
            (rightDownHit.collider != null && rightDownHit.collider.CompareTag("Floor")))
        {
            touchFloor = true;
        }
        else
        {
            touchFloor = false;
        }        
    }

    void MoveLeftAndRight()
    {
        if (touchWall)
        {
            faceRight = !faceRight;
            touchWall = false;
        }

        rigid.velocity = new Vector2(runSpeed * faceInt, rigid.velocity.y);

        if (faceRight)
        {
            transform.localScale = new Vector2(-1, 1);
            faceInt = 1;
        }
        else
        {
            transform.localScale = new Vector2(1, 1);
            faceInt = -1;
        }
    }

    public void Check(string TYPE)
    {
        if (TYPE == "WALL")
        {
            touchWall = true;
        }
        if (TYPE == "FLOOR")
        {
            touchWall = true;
        }
        if (TYPE == "PLAYER")
        {
            anim.SetTrigger("Die");
            gameObject.tag = "EnemyDied";
            runSpeed = 0;
            //playerController.JumpFromEnemy();
			Invoke ("Destroy", 0.5f);
        }
    }

	void Destroy()
	{
		Destroy (gameObject);
	}

    IEnumerator BlockEdgeDetecting()
    {
        blockEdgeCheck = true;
        yield return new WaitForSeconds(1);
        blockEdgeCheck = false;
    }
}
