﻿using UnityEngine;
using System.Collections;

public class EnemyCheckers : MonoBehaviour
{    
    public GameObject enemy;
    EnemyController enemyController;

    void Start()
    {
        enemyController = enemy.GetComponent<EnemyController>();
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
                enemyController.Check("WALL");
        }
        
        if (collision.gameObject.CompareTag("MPlayer"))
        {
            enemyController.Check("PLAYER");
        }
    }
}
