﻿using UnityEngine;
using System.Collections;

public class RopeChildScript : MonoBehaviour
{
    Rigidbody2D rigid;
    public float force = 5;

    void Awake()
    {
        rigid = GetComponent<Rigidbody2D>();
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("MPlayer"))
        {
            rigid.AddForce((transform.position - collision.transform.position) * force);
        }
    }
}
