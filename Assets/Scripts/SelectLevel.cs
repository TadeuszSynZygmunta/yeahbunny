﻿using UnityEngine;
using System.Collections;

public class SelectLevel : MonoBehaviour
{
    LevelSelectController lvlSelController;
    PlayerLvlSelectController playerLvlSelController;
    SavingScript savingScript;
    MainMenu mainMenu;

    public int levelNumber, wayPointNumber;
    public bool locked = false;
    public GameObject flagSprite, sparkle;
    Animator flagAnim;
    Vector2 spPos1, spPos2, spPos3;

    void Awake()
    {
        lvlSelController = FindObjectOfType<LevelSelectController>();
        playerLvlSelController = FindObjectOfType<PlayerLvlSelectController>();
        mainMenu = FindObjectOfType<MainMenu>();

        if (flagSprite != null)
            flagAnim = flagSprite.GetComponent<Animator>();

        savingScript = FindObjectOfType<SavingScript>();
        mainMenu.openLevelSelectEvent += ChangeFlagAnim;
    }

    void Start()
    {
        spPos1 = new Vector2(transform.localPosition.x - 9, transform.localPosition.y + 8);
        spPos2 = new Vector2(transform.localPosition.x + 2, transform.localPosition.y + 13);
        spPos3 = new Vector2(transform.localPosition.x + 8, transform.localPosition.y + 6);

        
        if (savingScript.levelsState[levelNumber] == 1)
        {
            if (savingScript.lastCompletedStage == levelNumber)
            {
                lvlSelController.target = levelNumber + 1;
                StartCoroutine("FlagAnimControl");
            }
        }
    }

    void ChangeFlagAnim()
    {
        if (savingScript.levelsState[levelNumber] == 1)
        {
            if (flagSprite != null)
                flagAnim.SetTrigger("unlocked");
        }
    }

    void OnMouseDown()
    {
        if (!locked)
        {
            lvlSelController.target = wayPointNumber;
        }       
    }

    IEnumerator FlagAnimControl()
    {
        yield return new WaitForSeconds(0.5f);
        if(flagSprite != null)
            flagAnim.SetTrigger("unlock");
        yield return new WaitForSeconds(0.6f);
        if(sparkle != null)
            Instantiate(sparkle, spPos1, Quaternion.identity);
        yield return new WaitForSeconds(0.6f);
        if (sparkle != null)
            Instantiate(sparkle, spPos2, Quaternion.identity);
        yield return new WaitForSeconds(0.6f);
        if (sparkle != null)
            Instantiate(sparkle, spPos3, Quaternion.identity);
    }
}
