﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerLvlSelectController : MonoBehaviour
{
    public delegate void PlayerLvlSelectDelegate();
    public PlayerLvlSelectDelegate selectLevelEvent;

    public GameObject activeLvl;
    public int selectedLvl;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("lvlWaypoint"))
        {
            activeLvl.transform.position = collision.transform.position;
            activeLvl.SetActive(true);
            selectedLvl = collision.gameObject.GetComponent<SelectLevel>().levelNumber;
            selectLevelEvent();
        } 
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("lvlWaypoint"))
        {
            activeLvl.SetActive(false);
        }
    }
}
