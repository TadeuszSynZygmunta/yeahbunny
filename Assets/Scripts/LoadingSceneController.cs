﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadingSceneController : MonoBehaviour
{
    public GameObject loadingScreen;
    ZoomingCircleController zoomingCircleController;
    SavingScript savingScript;

    private AsyncOperation async = null;
    public bool openOnStart = false;

    void Awake()
    {
        savingScript = FindObjectOfType<SavingScript>();
        zoomingCircleController = loadingScreen.GetComponent<ZoomingCircleController>();
    }

    void Start()
    {
        if (openOnStart || savingScript.startMenuFromMap)
        {
            zoomingCircleController.Open();
        }
    }

    void Update()
    {
        /* if(async != null)
         {
             Debug.Log(async.progress);
             if (Input.GetKeyDown(KeyCode.A))
             {
                 async.allowSceneActivation = true;
             }

             if (async.progress >= 0.9f)
             {
                 box.SetActive(true);
             }
         }  */
    }

    public void ShowLoadingScreen()
    {
        zoomingCircleController.Close();
    }

    public void StartLoading(int sceneNumber)
    {
        StartCoroutine(LoadLevel(sceneNumber));
    }

    private IEnumerator LoadLevel(int level)
    {
        ShowLoadingScreen();
        yield return new WaitForSeconds(1);
        async = SceneManager.LoadSceneAsync(level + 1);
        //async.allowSceneActivation = false;      
        yield return async;
    }
}
