﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SavingScript : MonoBehaviour {

    public List<int> levelsState, locksState;
    public List<int> keys;
    public int carrots, sumOfKeys;
    public bool startMenuFromMap = false;
    public int lastCompletedStage = 0;

    private static SavingScript instance;

    public static SavingScript Instance
    {
        get { return instance; }
    }

    void Awake()
    {
        levelsState = new List<int>();
        for (int i = 0; i < 48; i++)
        {
            levelsState.Add(0);
        }

        locksState = new List<int>();
        for (int i = 0; i < 8; i++)
        {
            locksState.Add(0);
        }

        keys = new List<int>();
        for (int i = 0; i < 48; i++)
        {
            keys.Add(0);
        }

        Load();
    }

	void Start ()
    {               
		Application.targetFrameRate = 60;
        DontDestroyOnLoad(gameObject);
	}

    public void SumKeys()
    {
        sumOfKeys = 0;

        for(int i = 0; i < 48; i++)
        {
            sumOfKeys += keys[i];
        }
    }

    public void Save()
    {
        SaveList("LevelsState", levelsState);
        Debug.Log("Levels saved!!");
        SaveList("LocksState", locksState);
        SaveList("Keys", keys);
        SaveInt("Carrots", carrots);        
    }

    public void Load()
    {
        levelsState = LoadList("LevelsState", 48);
        Debug.Log("Levels loaded!!");

        locksState = LoadList("LocksState", 8);
        Debug.Log("Locks loaded!!");

        keys = LoadList("Keys", 48);
        Debug.Log("Keys loaded!!");

        LoadInt("Carrots");
        SumKeys();
    }

    public void SaveFloat(string Name, float Value)
    {
        PlayerPrefs.SetFloat(Name, Value);
    }
    public float LoadFloat(string Name)
    {
        return PlayerPrefs.GetFloat(Name);
    }

    public void SaveString(string Name, string Value)
    {
        PlayerPrefs.SetString(Name, Value);
    }
    public string LoadString(string Name)
    {
        return PlayerPrefs.GetString(Name);
    }

    public void SaveInt(string Name, int Value)
    {
        PlayerPrefs.SetInt(Name, Value);
    }
    public int LoadInt(string Name)
    {
        return PlayerPrefs.GetInt(Name);
    }

    public void SaveDouble(string Name, double Value)
    {
        PlayerPrefs.SetString(Name, Value.ToString("R"));
    }
    public double LoadDouble(string Name)
    {
        double output = 0;
        double.TryParse(PlayerPrefs.GetString(Name), out output);
        return output;
    }

    public void SaveVector3(string Name, Vector3 Value)
    {
        PlayerPrefs.SetFloat(Name + "X", Value.x);
        PlayerPrefs.SetFloat(Name + "Y", Value.y);
        PlayerPrefs.SetFloat(Name + "Z", Value.z);
    }

    public Vector3 LoadVector3(string Name)
    {
        Vector3 vector3 = new Vector3();
        vector3.x = PlayerPrefs.GetFloat(Name + "X");
        vector3.y = PlayerPrefs.GetFloat(Name + "Y");
        vector3.z = PlayerPrefs.GetFloat(Name + "Z");
        return vector3;
    }

    public void SaveVector2(string Name, Vector2 Value)
    {
        PlayerPrefs.SetFloat(Name + "X", Value.x);
        PlayerPrefs.SetFloat(Name + "Y", Value.y);
    }

    public Vector2 LoadVector2(string Name)
    {
        Vector2 vector2 = new Vector2();
        vector2.x = PlayerPrefs.GetFloat(Name + "X");
        vector2.y = PlayerPrefs.GetFloat(Name + "Y");
        return vector2;
    }

    public void SaveList(string Name, List<int> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            PlayerPrefs.SetInt(Name + i.ToString(), list[i]);
        }
    }

    public List<int> LoadList(string Name, int lenght)
    {
        List<int> list = new List<int>();

        for (int i = 0; i < lenght; i++)
        {
            list.Add(PlayerPrefs.GetInt(Name + i.ToString()));
        }
        return list;
    }
}
