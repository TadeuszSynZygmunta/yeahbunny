﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelSelectController : MonoBehaviour {

    public List<GameObject> wayPoints, locks;
    int currentTarget, currentPosition, direction;
    public int target;
    public GameObject player;
    public float playerSpeed = 10;
    float distanceFactor;

    private Vector3 velocity = Vector3.zero;

    public ParticleSystem dust_runParticle;

    void Start () {
	    
	}
	
	void Update () {
	   if(target > currentPosition)
        {
            currentTarget = currentPosition + 1;
            direction = 1;            
        }
        else if(target < currentPosition)
        {
            currentTarget = currentPosition - 1;
            direction = -1;
        }

       if(currentPosition == currentTarget && currentPosition != target)
        {
            if(direction == 1)
            {
                currentPosition++;
            }else
            {
                currentPosition--;
            }           
        }

       if(currentPosition != target)
        {
            PlayerMovement();
            dust_runParticle.Play();
        }
        else
        {
            dust_runParticle.Stop();
        }

       if(direction != 0)
            player.transform.localScale = new Vector2(transform.localScale.x * direction, transform.localScale.y);    
	}

    void PlayerMovement()
    {        
        Vector2 currentTargetPos = new Vector2(wayPoints[currentTarget].transform.position.x + 1, wayPoints[currentTarget].transform.position.y + 5);
        player.transform.position = Vector3.MoveTowards(player.transform.position, currentTargetPos, playerSpeed * distanceFactor * Time.deltaTime);

        if ((Vector2)player.transform.position == currentTargetPos)
        {
            currentPosition = currentTarget;
        }
        distanceFactor = Mathf.Abs(currentPosition - target);
        distanceFactor ++;
        distanceFactor /= 2;
    }
}
