﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LockController : MonoBehaviour {

    Animator anim;
    LevelSelectController lvlSelectController;
    SavingScript savingScript;
    MainMenu mainMenu;

    public int keysToUnlock, lockNumber;
    bool unlocked = false;

    public List<GameObject> buttons;

    void Awake()
    {
        anim = GetComponent<Animator>();
        lvlSelectController = FindObjectOfType<LevelSelectController>();
        savingScript = FindObjectOfType<SavingScript>();
        mainMenu = FindObjectOfType<MainMenu>();
    }

	void OnEnable () {
        if(!mainMenu.menuOpen)
            CheckState();
	}

    void CheckState()
    {
        for (int i = 0; i < buttons.Count; i++)
        {
            buttons[i].GetComponent<SelectLevel>().locked = true;
        }
        if (savingScript.sumOfKeys >= keysToUnlock)
        {          
            anim.SetTrigger("unlocked");
            unlocked = true;
            if (savingScript.locksState[lockNumber] == 1)
            {
                gameObject.SetActive(false);
                for (int i = 0; i < buttons.Count; i++)
                {
                    buttons[i].GetComponent<SelectLevel>().locked = false;
                }
            }
        }
    }

    void OnMouseDown()
    {
        if (unlocked)
        {
            savingScript.locksState[lockNumber] = 1;
            savingScript.Save();
            anim.SetTrigger("clicked");

            for (int i = 0; i < buttons.Count; i++)
            {
                buttons[i].GetComponent<SelectLevel>().locked = false;
            }
        }    
    }
}
