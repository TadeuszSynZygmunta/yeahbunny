﻿using UnityEngine;
using System.Collections;

public class KeyController : MonoBehaviour {

    Animator anim;
    UI ui;

    void Awake()
    {
        anim = GetComponent<Animator>();
        ui = FindObjectOfType<UI>();
    }

    public void KeyCollected()
    {
        gameObject.tag = "KeyCollected";
        anim.SetTrigger("Exit");
        Destroy(gameObject, 1);
        ui.keysCollected++;
    }
}
