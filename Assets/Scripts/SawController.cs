﻿using UnityEngine;
using System.Collections;

public class SawController : MonoBehaviour {

    bool left, bounced;
    public bool vertical;

    public Vector2 leftBorder, rightBorder;
    public GameObject saw;
    Animator anim;

    public float speed = 0;
    void Awake()
    {
        anim = GetComponent<Animator>();
    }
    
    void Start () {
	
	}
	
	void Update () {

        if (vertical)
        {
            if (!left)
            {
                saw.transform.localPosition += transform.up * speed * Time.deltaTime;
            }
            else
            {
                saw.transform.localPosition -= transform.up * speed * Time.deltaTime;
            }

            if (saw.transform.localPosition.y <= leftBorder.y)
            {
                left = false;
                bounced = false;
            }
            if (saw.transform.localPosition.y >= rightBorder.y)
            {
                left = true;
                bounced = false;
            }

            if (saw.transform.localPosition.y <= leftBorder.y + 6 && left && !bounced)
            {
                bounced = true;
                anim.SetTrigger("Bounce");
            }
            else if (saw.transform.localPosition.y >= rightBorder.y - 6 && !left && !bounced)
            {
                bounced = true;
                anim.SetTrigger("Bounce");
            }
        }
        else
        {
            if (!left)
            {
                saw.transform.localPosition += transform.right * speed * Time.deltaTime;
            }
            else
            {
                saw.transform.localPosition -= transform.right * speed * Time.deltaTime;
            }

            if (saw.transform.localPosition.x <= leftBorder.x)
            {
                left = false;
                bounced = false;
            }
            if (saw.transform.localPosition.x >= rightBorder.x)
            {
                left = true;
                bounced = false;
            }

            if (saw.transform.localPosition.x <= leftBorder.x + 6 && left && !bounced)
            {
                bounced = true;
                anim.SetTrigger("Bounce");
            }
            else if (saw.transform.localPosition.x >= rightBorder.x - 6 && !left && !bounced)
            {
                bounced = true;
                anim.SetTrigger("Bounce");
            }
        }
        
    }
}
