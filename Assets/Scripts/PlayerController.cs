﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{
	public delegate void CollisionDelegate();
	public CollisionDelegate portalCollisionEvent;
	private GroundState groundState;

	public bool faceRight = true;
	public float setRunSpeed, jumpForce, slideSpeed;

	float runSpeed;
	int jumpNum = 0;

	public bool doubleJump = false, blockBackRaycast = false, playerStop = false, gameOver = false, gamePaused = false, lvlCompleted = false, stopMovement = false, mousekeyPressed = false;

	public Rigidbody2D rigid;
	RaycastHit2D leftDownHit, rightDownHit, rightTopHit, leftTopHit, frontHit1, frontHit2, frontHit3, backHit1, backHit2, backHit3;
	Animator anim;
	SpriteRenderer spriteRenderer;
	UI ui;

	Vector2 centerDown, centerCenter, centerTop, rightDown, leftDown, rightTop, leftTop;

	public GameObject redCircleParticle_prefab, jump_dustParticleL_prefab, jump_dustParticleR_prefab, doubleJump_dustParticleL_prefab, doubleJump_dustParticleR_prefab, wallJump_dustParticleL_prefab, wallJump_dustParticleR_prefab, tapToStartText;
	List<GameObject> circleParticle, jump_dustParticleL, jump_dustParticleR, doubleJump_dustParticleL, doubleJump_dustParticleR, wallJump_dustParticleL, wallJump_dustParticleR;
	public ParticleSystem dust_runParticle;

	void Awake()
	{
		rigid = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();
		spriteRenderer = GetComponent<SpriteRenderer>();
		ui = FindObjectOfType<UI>();

		circleParticle = new List<GameObject>();
		jump_dustParticleL = new List<GameObject>();
		jump_dustParticleR = new List<GameObject>();
		doubleJump_dustParticleL = new List<GameObject>();
		doubleJump_dustParticleR = new List<GameObject>();
		wallJump_dustParticleL = new List<GameObject>();
		wallJump_dustParticleR = new List<GameObject>();
		groundState = new GroundState(transform.gameObject);
	}

	void Start()
	{
		AddToLists();
		runSpeed = setRunSpeed;
		playerStop = true;
		rigid.isKinematic = true;
		dust_runParticle.Stop();
	}

	void FixedUpdate()
	{
		if (!playerStop) {
			if (!stopMovement) {
				MoveLeftAndRight ();
			}
		}
	}

	void Update()
	{
		if (!playerStop)
		{
			if (!stopMovement)
			{
				//MoveLeftAndRight();
			}           
			if (!lvlCompleted)
			{
				Jump();
				WallJump();
			}     

			Animations();
		}

		//if (playerStop && Input.GetKeyDown(KeyCode.D))
		if (playerStop && Input.GetMouseButtonDown(0))
		{
			if (!gameOver && !lvlCompleted && !gamePaused)
			{
				playerStop = false;
				rigid.isKinematic = false;
				tapToStartText.SetActive(false);
				ui.ShowPauseButton();
			}
		}
	}
		
	void MoveLeftAndRight()
	{
		rigid.velocity = new Vector2(runSpeed * groundState.faceInt, rigid.velocity.y);

		if (groundState.isFrontWall())
		{
			faceRight = !faceRight;
			blockBackRaycast = false;
		}

		if (groundState.isBackWall() && !groundState.isGround() && !blockBackRaycast/* && !touchCeiling*/)
		{
			runSpeed = 0;
			rigid.velocity = new Vector2(rigid.velocity.x, -slideSpeed);
		}
		else
		{
			runSpeed = setRunSpeed;
		}
			
		if (faceRight)
		{
			//transform.localScale = new Vector2(1, 1);
			spriteRenderer.flipX = false;
			groundState.faceInt = 1;
		}
		else
		{
			//transform.localScale = new Vector2(-1, 1);
			spriteRenderer.flipX = true;
			groundState.faceInt = -1;
		}

		if (groundState.isGround() || groundState.isBackWall())
		{
			dust_runParticle.Play();
		}else
		{
			dust_runParticle.Stop();
		}
	}

	void Jump()
	{
		if (groundState.isGround()/* && !touchCeiling*/)
		{
			doubleJump = false;
			runSpeed = setRunSpeed;

			//if (Input.GetKeyDown(KeyCode.D))
			if (Input.GetMouseButtonDown(0))
			{
				if (!EventSystem.current.IsPointerOverGameObject())
				{
					blockBackRaycast = false;
					rigid.velocity = new Vector2(rigid.velocity.x, jumpForce);
					if (faceRight)
					{
						SpawnParticle("jumpL");
					}
					else
					{
						SpawnParticle("jumpR");
					}
				}          
			}
		}
		else
		{
			//if (!doubleJump && !wallJump && Input.GetKeyDown(KeyCode.D))
			if (!doubleJump && !groundState.isFrontWall() && !groundState.isBackWall() && Input.GetMouseButtonDown(0))
			{
				if (!EventSystem.current.IsPointerOverGameObject())
				{
					blockBackRaycast = false;
					rigid.velocity = new Vector2(rigid.velocity.x, jumpForce);
					doubleJump = true;
					if (faceRight)
					{
						SpawnParticle("dJumpL");
					}
					else
					{
						SpawnParticle("dJumpR");
					}
				}
			}
		}
	}

	void WallJump()
	{
		if (groundState.isBackWall() && !groundState.isGround())
		{
			doubleJump = false;
			//if (Input.GetKeyDown(KeyCode.D))
			if(Input.GetMouseButtonDown(0))
			{
				if (!EventSystem.current.IsPointerOverGameObject())
				{
					blockBackRaycast = true;
					runSpeed = setRunSpeed;
					rigid.velocity = new Vector2(rigid.velocity.x, jumpForce);
					if (faceRight)
					{
						SpawnParticle("wJumpL");
					}
					else
					{
						SpawnParticle("wJumpR");
					}
				}
			}
		}
	}

	public void JumpFromEnemy()
	{
		doubleJump = false;
		runSpeed = setRunSpeed;
		//if (Input.GetKeyDown(KeyCode.D))
		blockBackRaycast = false;
		rigid.velocity = new Vector2(rigid.velocity.x, jumpForce / 2);
		if (faceRight)
		{
			SpawnParticle("dJumpL");
		}
		else
		{
			SpawnParticle("dJumpR");
		}
		Debug.Log("DODODODOODODODOD");
	}

	void Animations()
	{
		anim.SetFloat("ySpeed", rigid.velocity.y);
		anim.SetBool("touchFloor", groundState.isGround());
		anim.SetBool("touchWall", groundState.isFrontWall());
		anim.SetBool("wallSlide", groundState.isBackWall());
		anim.SetInteger("doubleJump", jumpNum);
	}

	public void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.CompareTag("Portal"))
		{
			portalCollisionEvent();
		}

		if (collision.gameObject.CompareTag("Key"))
		{
			collision.GetComponent<KeyController>().KeyCollected();                        
		}

		if (collision.gameObject.CompareTag("Carrot"))
		{
			collision.GetComponent<CarrotController>().KeyCollected();           
		}
	}

	public void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.CompareTag("Obstacle"))
		{
			Invoke("HidePlayer", 0.2f);

			if (!gameOver)
			{
				Instantiate(redCircleParticle_prefab, transform.position, Quaternion.identity);
			}
			gameOver = true;
			ui.ShowGameOverPanel();
			anim.SetTrigger("Death");
			anim.SetBool("Dead", true);
		}
	}

	public void HidePlayer()
	{
		rigid.isKinematic = true;
		transform.localScale = new Vector3(0, 0, 0);
		playerStop = true;
		dust_runParticle.Stop();
	}

	void SpawnParticle(string type)
	{
		switch (type)
		{
		case "circle":
			for (int i = 0; i < 5; i++)
			{
				if (!circleParticle[i].activeInHierarchy)
				{
					circleParticle[i].transform.position = transform.position;
					circleParticle[i].SetActive(true);
					break;
				}
			}
			break;
		case "jumpL":
			for (int i = 0; i < 5; i++)
			{
				if (!jump_dustParticleL[i].activeInHierarchy)
				{
					jump_dustParticleL[i].transform.position = new Vector2(transform.position.x - 11.5f, transform.position.y + 7.0f);
					jump_dustParticleL[i].SetActive(true);
					break;
				}
			}
			break;
		case "jumpR":
			for (int i = 0; i < 5; i++)
			{
				if (!jump_dustParticleR[i].activeInHierarchy)
				{
					jump_dustParticleR[i].transform.position = new Vector2(transform.position.x + 11.5f, transform.position.y + 7.0f);
					jump_dustParticleR[i].SetActive(true);
					break;
				}
			}
			break;
		case "dJumpL":
			for (int i = 0; i < 5; i++)
			{
				if (!doubleJump_dustParticleL[i].activeInHierarchy)
				{
					doubleJump_dustParticleL[i].transform.position = new Vector2(transform.position.x - 13.5f, transform.position.y - 14.5f);
					doubleJump_dustParticleL[i].SetActive(true);
					break;
				}
			}
			break;
		case "dJumpR":
			for (int i = 0; i < 5; i++)
			{
				if (!doubleJump_dustParticleR[i].activeInHierarchy)
				{
					doubleJump_dustParticleR[i].transform.position = new Vector2(transform.position.x + 13.5f, transform.position.y - 14.5f);
					doubleJump_dustParticleR[i].SetActive(true);
					break;
				}
			}
			break;
		case "wJumpL":
			for (int i = 0; i < 5; i++)
			{
				if (!wallJump_dustParticleL[i].activeInHierarchy)
				{
					wallJump_dustParticleL[i].transform.position = new Vector2(transform.position.x + 9.5f, transform.position.y - 12.5f);
					wallJump_dustParticleL[i].SetActive(true);
					break;
				}
			}
			break;
		case "wJumpR":
			for (int i = 0; i < 5; i++)
			{
				if (!wallJump_dustParticleR[i].activeInHierarchy)
				{
					wallJump_dustParticleR[i].transform.position = new Vector2(transform.position.x - 9.5f, transform.position.y - 12.5f);
					wallJump_dustParticleR[i].SetActive(true);
					break;
				}
			}
			break;
		default:
			break; 
		}     
	}

	void AddToLists()
	{
		for(int i = 0; i < 5; i++)
		{
			GameObject obj2 = (GameObject)Instantiate(jump_dustParticleL_prefab, new Vector2(0, 0), Quaternion.identity);
			jump_dustParticleL.Add(obj2);
			obj2.SetActive(false);

			GameObject obj3 = (GameObject)Instantiate(jump_dustParticleR_prefab, new Vector2(0, 0), Quaternion.identity);
			jump_dustParticleR.Add(obj3);
			obj3.SetActive(false);

			GameObject obj4 = (GameObject)Instantiate(doubleJump_dustParticleL_prefab, new Vector2(0, 0), Quaternion.identity);
			doubleJump_dustParticleL.Add(obj4);
			obj4.SetActive(false);

			GameObject obj5 = (GameObject)Instantiate(doubleJump_dustParticleR_prefab, new Vector2(0, 0), Quaternion.identity);
			doubleJump_dustParticleR.Add(obj5);
			obj5.SetActive(false);

			GameObject obj6 = (GameObject)Instantiate(wallJump_dustParticleL_prefab, new Vector2(0, 0), Quaternion.identity);
			wallJump_dustParticleL.Add(obj6);
			obj6.SetActive(false);

			GameObject obj7 = (GameObject)Instantiate(wallJump_dustParticleR_prefab, new Vector2(0, 0), Quaternion.identity);
			wallJump_dustParticleR.Add(obj7);
			obj7.SetActive(false);
		}
	}

	void StopMovement()
	{
		stopMovement = true;
	}

	//=======================================

	public class GroundState
	{
		private GameObject player;
		private float  width;
		private float height;
		private float length;
		public int faceInt = 1;

		//GroundState constructor.  Sets offsets for raycasting.
		public GroundState(GameObject playerRef)
		{
			player = playerRef;
			width = player.GetComponent<Collider2D>().bounds.extents.x + 0.1f;
			height = player.GetComponent<Collider2D>().bounds.extents.y + 0.2f - player.GetComponent<Collider2D>().offset.y;
			length = 2f;
		}

		//Returns whether or not player is touching wall.
		public bool isFrontWall()
		{
			Vector2 front_top_pos = new Vector2(player.transform.position.x + (width * faceInt), player.transform.position.y + height - 0.7f);
			Vector2 front_cen_pos = new Vector2(player.transform.position.x + (width * faceInt), player.transform.position.y);
			Vector2 front_dow_pos = new Vector2(player.transform.position.x + (width * faceInt), player.transform.position.y - height + 0.2f);
			RaycastHit2D front_top_hit, front_cen_hit, front_dow_hit;

			front_top_hit = Physics2D.Raycast(front_top_pos, Vector2.right * faceInt, length / 2);
			front_cen_hit = Physics2D.Raycast(front_cen_pos, Vector2.right * faceInt, length / 2);
			front_dow_hit = Physics2D.Raycast(front_dow_pos, Vector2.right * faceInt, length / 2);

			Debug.DrawLine (front_top_pos, new Vector2(front_top_pos.x + (length / 2 * faceInt), front_top_pos.y), Color.red);
			Debug.DrawLine (front_cen_pos, new Vector2(front_cen_pos.x + (length / 2 * faceInt), front_cen_pos.y), Color.red);
			Debug.DrawLine (front_dow_pos, new Vector2(front_dow_pos.x + (length / 2 * faceInt), front_dow_pos.y), Color.red);

			if((front_top_hit.collider != null && front_top_hit.collider.gameObject.CompareTag("Floor")) ||
			   (front_cen_hit.collider != null && front_cen_hit.collider.gameObject.CompareTag("Floor")) ||
		       (front_dow_hit.collider != null && front_dow_hit.collider.gameObject.CompareTag("Floor")))
				return true;
			else
				return false;
		}

		public bool isBackWall()
		{
			Vector2 back_top_pos = new Vector2(player.transform.position.x - (width * faceInt), player.transform.position.y + height - 0.7f);
			Vector2 back_cen_pos = new Vector2(player.transform.position.x - (width * faceInt), player.transform.position.y);
			Vector2 back_dow_pos = new Vector2(player.transform.position.x - (width * faceInt), player.transform.position.y - height + 0.2f);

			RaycastHit2D back_top_hit, back_cen_hit, back_dow_hit;

			back_top_hit = Physics2D.Raycast(back_top_pos, -Vector2.right * faceInt, length);
			back_cen_hit = Physics2D.Raycast(back_cen_pos, -Vector2.right * faceInt, length);
			back_dow_hit = Physics2D.Raycast(back_dow_pos, -Vector2.right * faceInt, length);

			Debug.DrawLine (back_top_pos, new Vector2(back_top_pos.x - (length * faceInt), back_top_pos.y), Color.red);
			Debug.DrawLine (back_cen_pos, new Vector2(back_cen_pos.x - (length * faceInt), back_cen_pos.y), Color.red);
			Debug.DrawLine (back_dow_pos, new Vector2(back_dow_pos.x - (length * faceInt), back_dow_pos.y), Color.red);

			if((back_top_hit.collider != null && back_top_hit.collider.gameObject.CompareTag("Floor")) ||
			   (back_cen_hit.collider != null && back_cen_hit.collider.gameObject.CompareTag("Floor")) ||
			   (back_dow_hit.collider != null && back_dow_hit.collider.gameObject.CompareTag("Floor")))
				return true;
			else
				return false;
		}

		//Returns whether or not player is touching ground.
		public bool isGround()
		{			
			Vector2 bottom1_pos = new Vector2 (player.transform.position.x, player.transform.position.y - height);
			Vector2 bottom2_pos = new Vector2(player.transform.position.x + (width - 0.2f), player.transform.position.y - height);
			Vector2 bottom3_pos = new Vector2(player.transform.position.x - (width - 0.2f), player.transform.position.y - height);
			RaycastHit2D bottom1_hit, bottom2_hit, bottom3_hit;

			bottom1_hit = Physics2D.Raycast(bottom1_pos, -Vector2.up, length);
			bottom2_hit = Physics2D.Raycast(bottom2_pos, -Vector2.up, length);
			bottom3_hit = Physics2D.Raycast(bottom3_pos, -Vector2.up, length);

			Debug.DrawLine (bottom1_pos, bottom1_hit.point, Color.red);
			Debug.DrawLine (bottom2_pos, bottom2_hit.point, Color.red);
			Debug.DrawLine (bottom3_pos, bottom3_hit.point, Color.red);

			if((bottom1_hit.collider != null && bottom1_hit.collider.gameObject.CompareTag("Floor")) || 
			   (bottom2_hit.collider != null && bottom2_hit.collider.gameObject.CompareTag("Floor")) ||
			   (bottom3_hit.collider != null && bottom3_hit.collider.gameObject.CompareTag("Floor")))
				return true;
			else
				return false;
		}
	}
}