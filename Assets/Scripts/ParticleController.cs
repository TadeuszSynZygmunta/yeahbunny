﻿using UnityEngine;
using System.Collections;

public class ParticleController : MonoBehaviour {

    Animator anim;
    public float setactiveTime = 0.5f;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }

    void SetGOActive()
    {
        gameObject.SetActive(false);
    }

    void OnEnable()
    {
        anim.SetTrigger("Start");
        Invoke("SetGOActive", setactiveTime);
    }

    void OnDisable()
    {
        anim.SetTrigger("Stop");
    }
}
