﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PortalController : MonoBehaviour {

    Animator anim;
    PlayerController playerController;
    SavingScript savingScript;
    UI ui;
       
    public int sceneNumber;

    void Awake()
    {
        anim = GetComponent<Animator>();
        playerController = FindObjectOfType<PlayerController>();
        savingScript = FindObjectOfType<SavingScript>();
        ui = FindObjectOfType<UI>();
    }

	void Start () {
        playerController.portalCollisionEvent += LvLCompleted;
	}

    void LvLCompleted()
    {
        anim.SetTrigger("Close");
        playerController.lvlCompleted = true;
        savingScript.levelsState[sceneNumber] = 1;
        savingScript.Save();
        ui.Invoke("ShowEndLevelPanel", 0.5f);
        playerController.Invoke("StopMovement", 2);
    }

    void OnDisable()
    {
        playerController.portalCollisionEvent -= LvLCompleted;
    }
}
