﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    public delegate void MenuDelegate();
    public MenuDelegate openLevelSelectEvent;

    SavingScript savingScript;
    LoadingSceneController loadingSceneController;
    PlayerLvlSelectController playerLvlSelectController;

    public List<GameObject> buttons;

    public GameObject bunny, mainMenuPanel, mainMenuGameObject, levelSelectPanel, levelSelectGameObject, settingsPanel, zoomingCircle, logo;
    Animator bunnyAnim;
    public GameObject circleBackground, darkBackground, leaves;
    public int currentLevel;

    bool animFinished = false;
    public bool menuOpen = true;
    ZoomingCircleController zoomingCircleController;
    public Color cameraBackLight, cameraBackDark;
	public Text keysText;

    void Awake()
    {
        savingScript = FindObjectOfType<SavingScript>();
        bunnyAnim = bunny.GetComponent<Animator>();
        zoomingCircleController = zoomingCircle.GetComponent<ZoomingCircleController>();
        loadingSceneController = FindObjectOfType<LoadingSceneController>();
        playerLvlSelectController = FindObjectOfType<PlayerLvlSelectController>();
    }

	void Start ()
    {
        logo.SetActive(false);
        if (savingScript.startMenuFromMap)
        {
            levelSelectPanel.SetActive(true);
            levelSelectGameObject.SetActive(true);
            SetCameraBackgroundColorToLight();
            mainMenuGameObject.SetActive(false);
            mainMenuPanel.SetActive(false);
            menuOpen = false;
            animFinished = true;
            keysText.text = savingScript.sumOfKeys.ToString();
        }
        else
        {
            levelSelectPanel.SetActive(false);
            levelSelectGameObject.SetActive(false);
            SetCameraBackgroundColorToDark();

        }     
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (!animFinished)
            {                
                UnlockAnim();
            }

            if(animFinished && menuOpen)
            {
                Invoke("HideMainMenu", 0.8f);
                zoomingCircleController.Close();
                menuOpen = false;
            }
        }      
    }

    public void SelectLevel()
    {
        loadingSceneController.StartLoading(playerLvlSelectController.selectedLvl);
    }

    void UnlockAnim()
    {
        bunnyAnim.SetTrigger("Start");
        Invoke("SetBackgroundActive", 0.1f);
        Invoke("SetBackgroundUnactive", 0.0625f);
        Invoke("SpawnLeaves", 0.6f);
        Invoke("SpawnLeaves", 0.8f);
    }

    void ResetAnim()
    {
        darkBackground.SetActive(true);
        circleBackground.SetActive(false);
    }

    void SetBackgroundActive()
    {
        animFinished = true;
        circleBackground.SetActive(true);
        mainMenuPanel.SetActive(true);
        logo.SetActive(true);
    }

    void SetBackgroundUnactive()
    {
        darkBackground.SetActive(false);
    }

    void SetCameraBackgroundColorToLight()
    {
        Camera.main.backgroundColor = cameraBackLight;
    }

    void SetCameraBackgroundColorToDark()
    {
        Camera.main.backgroundColor = cameraBackDark;
    }

    void SpawnLeaves()
    {
        Instantiate(leaves, new Vector2(45, 108), Quaternion.identity);
    }

    public void BackButton()
    {
        Invoke("HideLevelSelect", 0.8f);
        zoomingCircleController.Close();
    }

    public void HideLevelSelect()
    {
        levelSelectGameObject.transform.position = new Vector2(0, transform.position.y);
        levelSelectPanel.SetActive(false);
        levelSelectGameObject.SetActive(false);
        mainMenuGameObject.SetActive(true);
        animFinished = false;
        menuOpen = true;
        ResetAnim();
        SetCameraBackgroundColorToDark();
    }

    public void HideMainMenu()
    {
        levelSelectPanel.SetActive(true);
        levelSelectGameObject.SetActive(true);
        keysText.text = savingScript.sumOfKeys.ToString();
        mainMenuPanel.SetActive(false);
        mainMenuGameObject.SetActive(false);
        logo.SetActive(false);
        SetCameraBackgroundColorToLight();
        if (openLevelSelectEvent != null)
            openLevelSelectEvent();       
    }

    public void SettingsButton()
    {        
        zoomingCircleController.Close();
        Invoke("ShowSettings", 0.8f);
    }

    public void BackSettingsButton()
    {
        zoomingCircleController.Close();
        Invoke("HideSettings", 0.8f);
    }

    void ShowSettings()
    {
        levelSelectPanel.SetActive(false);
        levelSelectGameObject.SetActive(false);
        settingsPanel.SetActive(true);
    }

    void HideSettings()
    {
        levelSelectPanel.SetActive(true);
        levelSelectGameObject.SetActive(true);
        settingsPanel.SetActive(false);
    }

    //===========SETTINGS===BUTTONS=================

    public void Mail()
    {
        Application.OpenURL("mailto:adzarzycki@gmail.com");
    }

    public void TwitterAZ()
    {
        Application.OpenURL("http://twitter.com/ZarzyckiAdrian");
    }
}