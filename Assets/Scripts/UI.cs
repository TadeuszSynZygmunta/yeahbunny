﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI : MonoBehaviour {

    PortalController portalController;
    SavingScript savingScript;
    LoadingSceneController loadingSceneController;
    PlayerController playerController;

    public GameObject gameOverPanel, endLevelPanel, pausePanel, pausebutton, lvlEndKey1, lvlEndKey2, lvlEndKey3,
        carrotCounter;
    Animator gameOverPanelAnim, endLevelPanelAnim, pausePanelAnim, lvlEndKey1Anim, lvlEndKey2Anim, lvlEndKey3Anim,
        carrotCounterAnim, pauseButtonAnim;

    public int levelCarrots;
    public int carrotsCollected, keysCollected;

    public Text carrotCounterTxt;
    bool carrotCounterShowed = false;
    int carrotCounterInt = 0;

    void Awake()
    {
        portalController = FindObjectOfType<PortalController>();
        savingScript = FindObjectOfType<SavingScript>();
        loadingSceneController = FindObjectOfType<LoadingSceneController>();
        playerController = FindObjectOfType<PlayerController>();
        gameOverPanelAnim = gameOverPanel.GetComponent<Animator>();
        endLevelPanelAnim = endLevelPanel.GetComponent<Animator>();
        pausePanelAnim = pausePanel.GetComponent<Animator>();
        lvlEndKey1Anim = lvlEndKey1.GetComponent<Animator>();
        lvlEndKey2Anim = lvlEndKey2.GetComponent<Animator>();
        lvlEndKey3Anim = lvlEndKey3.GetComponent<Animator>();
        carrotCounterAnim = carrotCounter.GetComponent<Animator>();
        pauseButtonAnim = pausebutton.GetComponent<Animator>();
    }

    void Start () {
        endLevelPanel.SetActive(false);
        gameOverPanel.SetActive(false);
        pausePanel.SetActive(false);
        pausebutton.SetActive(false);
        carrotCounterTxt.text = 0.ToString();
        carrotCounter.SetActive(false);
    }
	
	void Update () {

        if (carrotCounterInt <= 0 && carrotCounterShowed)
        {
            carrotCounterAnim.SetTrigger("Hide");
            Invoke("CCounterDisable", 1);
            carrotCounterInt = 0;
            carrotCounterShowed = false;
        }
        if(carrotCounterInt > 0)
        {
            carrotCounterInt--;
        }
    }

    public void Restart()
    {
        savingScript.Save();
        loadingSceneController.StartLoading(portalController.sceneNumber);     
    }

    public void BackToMap()
    {
        savingScript.startMenuFromMap = true;
        savingScript.lastCompletedStage = 0;
        savingScript.Save();
        loadingSceneController.StartLoading(0);
    }

    public void NextLevel()
    {
        savingScript.startMenuFromMap = true;
        savingScript.lastCompletedStage = portalController.sceneNumber;
        if(keysCollected > savingScript.keys[portalController.sceneNumber])
        {
            savingScript.keys[portalController.sceneNumber] = keysCollected;
        }
        savingScript.Save();
        loadingSceneController.StartLoading(0);
    }

    public void ShowEndLevelPanel()
    {
        endLevelPanel.SetActive(true);
        HidePauseButton();
        endLevelPanelAnim.SetTrigger("Show");
        StartCoroutine("EndLvlAnims");
    }

    public void ShowGameOverPanel()
    {
        gameOverPanel.SetActive(true);
        HidePauseButton();
        gameOverPanelAnim.SetTrigger("Show");
    }

    public void ShowPausePanel()
    {
        pausePanel.SetActive(true);
        HidePauseButton();
        playerController.playerStop = true;
        playerController.gamePaused = true;
        pausePanelAnim.SetTrigger("Show");        
    }

    public void HidePausePanel()
    {        
        pausePanelAnim.SetTrigger("Hide");       
        Invoke("UnactivePausePanel", 0.8f);
    }

    void UnactivePausePanel()
    {
        pausePanel.SetActive(false);
        ShowPauseButton();
        playerController.gamePaused = false;
        playerController.playerStop = false;
    }

    public void ShowPauseButton()
    {
        pausebutton.SetActive(true);
        pauseButtonAnim.SetTrigger("Show");
    }

    public void HidePauseButton()
    {
        pauseButtonAnim.SetTrigger("Hide");
    }

    IEnumerator EndLvlAnims()
    {
        yield return new WaitForSeconds(0.2f);

        if (keysCollected > 0)
        {
            yield return new WaitForSeconds(0.5f);
            lvlEndKey1Anim.SetTrigger("unlock");
        }
        if (keysCollected > 1)
        {
            yield return new WaitForSeconds(0.5f);
            lvlEndKey2Anim.SetTrigger("unlock");
        }
        if (keysCollected > 2)
        {
            yield return new WaitForSeconds(0.5f);
            lvlEndKey3Anim.SetTrigger("unlock");
        }
    }

    public void UpdateCarrotCounter()
    {
        carrotCounterTxt.text = "x" + carrotsCollected.ToString();
    }

    public void ShowCarrotCounter()
    {

        carrotCounter.SetActive(true);
        carrotCounterAnim.SetTrigger("Show");                                       
    }

    public void HideCarrotCounter()
    {
        carrotCounterAnim.SetTrigger("Hide");
        Invoke("CCounterDisable", 1);
    }

    void CCounterDisable()
    {
        carrotCounter.SetActive(false);
    }
}
