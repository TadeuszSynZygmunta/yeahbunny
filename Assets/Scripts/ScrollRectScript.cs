﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ScrollRectScript : MonoBehaviour
{
    private Vector3 panelStartPoint;
    public float panelOffsetLEFT, panelOffsetRIGHT, snapDistance;

    bool panelMoving = false, snap = false;
    float minMoveDistance = Screen.width / 50;
    Vector2 lastPanelPosition, snapPosition;

    public List<Vector2> snapPositions;

    public float speed, snapSpeed;

	private Vector3 velocity = Vector3.zero;

    void Update()
    {
        /*
        if (Input.touchCount == 1)
        {
            Touch currentTouch = Input.GetTouch(0);

            if (currentTouch.phase == TouchPhase.Canceled || currentTouch.phase == TouchPhase.Ended)
            {
                panelMoving = false;
            }
          
            if (currentTouch.phase == TouchPhase.Began)
            {
                panelStartPoint = currentTouch.position;
                lastPanelPosition = panelStartPoint;
            }

            if (currentTouch.phase == TouchPhase.Moved)
            {
                if (!panelMoving && Vector2.Distance(panelStartPoint, Input.GetTouch(0).position) > minMoveDistance)
                {
                    panelMoving = true;
                }

                if (panelMoving)
                {
                    Vector2 newTouchPosition = Input.GetTouch(0).position;
                    transform.position += transform.TransformDirection((Vector3)(lastPanelPosition - newTouchPosition));
                    lastPanelPosition = newTouchPosition;
                }
            }
        }
        */
        if (Input.GetMouseButtonDown(0))
        {
			panelStartPoint =  Camera.main.ScreenToWorldPoint(Input.mousePosition);
            lastPanelPosition = panelStartPoint;
        }

        if (Input.GetMouseButton(0))
        {
            if (!panelMoving && Vector2.Distance(panelStartPoint, Input.mousePosition) > minMoveDistance)
            {
                panelMoving = true;
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            panelMoving = false;
        }

		if (panelMoving) {
			Vector2 newTouchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			transform.position -= transform.TransformDirection ((Vector3)(new Vector2 (lastPanelPosition.x, transform.position.y) - new Vector2 (newTouchPosition.x, transform.position.y)));
			transform.position -= transform.TransformDirection((Vector3)((new Vector2(lastPanelPosition.x, transform.position.y) - new Vector2(newTouchPosition.x, transform.position.y)) * Camera.main.orthographicSize / Camera.main.pixelWidth * 2f));
			lastPanelPosition = newTouchPosition;

			//Vector2 newTouchPosition = Input.GetTouch(0).position;
			//transform.position += transform.TransformDirection((Vector3)((lastCameraPosition - newTouchPosition) * Camera.main.orthographicSize / Camera.main.pixelHeight * 2f));
			//lastCameraPosition = newTouchPosition;
		} 
		else 
		{
			SnapToPosition ();
		}      
    }

    void SnapToPosition()
    {
        for(int i = 0; i < snapPositions.Count; i++)
        {
            float distance = Vector2.Distance(transform.position, snapPositions[i]);

            if (distance <= snapDistance)
            {
                snapPosition = snapPositions[i];
                break;
            }
        }
        transform.position = Vector2.Lerp(transform.position, snapPosition, snapSpeed * Time.deltaTime);        
    }
       

    // convert screen point to world point
    private Vector2 getWorldPoint(Vector2 screenPoint)
    {
        RaycastHit hit;
        Physics.Raycast(Camera.main.ScreenPointToRay(screenPoint), out hit);
        return hit.point;
    }
}
