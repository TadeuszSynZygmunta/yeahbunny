﻿using UnityEngine;
using System.Collections;

public class CarrotController : MonoBehaviour {

    Animator anim;
    UI ui;

    void Awake()
    {
        anim = GetComponent<Animator>();
        ui = FindObjectOfType<UI>();
    }

    public void KeyCollected()
    {
        gameObject.tag = "CarrotCollected";
        anim.SetTrigger("Pick");
        Destroy(gameObject, 0.8f);
        ui.carrotsCollected++;
    }
}
