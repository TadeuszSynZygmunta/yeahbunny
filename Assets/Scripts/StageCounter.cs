﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StageCounter : MonoBehaviour {

    public GameObject numberSpriteGO;
    public List<Sprite> numberSprites;
    public int levelNumber, visibilityMin, visibilityMax, numberOfStagesInWorld;
    public bool visible = false;

    SpriteRenderer spriteRen;
    PlayerLvlSelectController plLvlSelController;
    Animator anim;

    void Awake()
    {
        spriteRen = numberSpriteGO.GetComponent<SpriteRenderer>();
        plLvlSelController = FindObjectOfType<PlayerLvlSelectController>();
        anim = GetComponent<Animator>();
    }

	void Start () {
        plLvlSelController.selectLevelEvent += ChangeNumber;
	}
	
	void Update () {
	
	}
    
    void ChangeNumber()
    {
        if(plLvlSelController.selectedLvl >= visibilityMin && plLvlSelController.selectedLvl <= visibilityMax)
        {
            visible = true;
            OnVisible();
        }else
        {
            visible = false;
            OnInvisible();
        }

        if (visible)
        {
            for(int i = visibilityMin; i < visibilityMax + 1; i++)
            {
                if (plLvlSelController.selectedLvl == i)
                {
                    levelNumber = i - visibilityMin;
                }
            }
           
            spriteRen.sprite = numberSprites[levelNumber + 1];
        }                     
    }

    void OnVisible()
    {
        gameObject.SetActive(true);
        anim.SetTrigger("Show");
    }

    void OnInvisible()
    {
        anim.SetTrigger("Hide");
        Invoke("HideGO", 0.3f);
    }

    void HideGO()
    {
        gameObject.SetActive(false);
    }
}
