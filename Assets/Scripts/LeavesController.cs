﻿using UnityEngine;
using System.Collections;

public class LeavesController : MonoBehaviour {

    public float speed;

	void OnEnable () {
        FindObjectOfType<MainMenu>().openLevelSelectEvent += DestroyGO;
        Invoke("DestroyGO", 5f);
    }
	
	void Update () {
        transform.position += new Vector3(0, speed * Time.deltaTime, 0);
	}

    void DestroyGO()
    {
        FindObjectOfType<MainMenu>().openLevelSelectEvent -= DestroyGO;
        Destroy(gameObject);
    }
}
